# Nuit du Code - 29 Mai 2024

<p align="center">
  <a href="https://www.univ-reunion.fr" target="_blank">
    <img src="https://www.univ-reunion.fr/wp-content/uploads/2022/10/Logo_UR.png" width="400" alt="Université de la Réunion">
  </a>
</p>

Bienvenue sur le dépôt GitLab officiel de la Nuit du Code, un événement dédié aux étudiants qui se déroulera le **29 mai 2024 de 15h00 à 21h00**. Pendant ces 6 heures, les étudiants coderont sans interruption pour relever un défi passionnant !

## Équipes

Les étudiants sont organisés en équipes. Voici la liste des équipes participantes :

1. **Équipe 1 : The 3 Little Pyxels**
2. **Équipe 2 : [Nom de l'équipe]**
3. **Équipe 3 : [Nom de l'équipe]**
4. **Équipe 4 : [Nom de l'équipe]**

## Défi

<p align="center">
  <a href="https://github.com/kitao/pyxel" target="_blank">
    <img src="https://raw.githubusercontent.com/kitao/pyxel/main/docs/images/pyxel_logo_152x64.png" width="152" alt="Logo de Pyxel">
  </a>
</p>

Le défi de cette année consiste à utiliser [Pyxel](https://github.com/kitao/pyxel) pour créer un jeu vidéo en seulement 6 heures. Pyxel est un moteur de jeu rétro facile à utiliser, parfait pour ce genre de compétition rapide.

## En savoir plus

Pour plus d'informations sur la Nuit du Code, visitez le [site officiel](https://www.nuitducode.net/).

## Règles et critères de jugement

- **Originalité** : Votre jeu doit être original et créatif.
- **Jouabilité** : Le jeu doit être jouable sans bugs majeurs.
- **Esthétique** : Les graphismes et les sons doivent être attrayants.
- **Respect du thème** : Le jeu doit correspondre au thème donné au début de l'événement.


---

<p align="center">
  <a href="https://www.nuitducode.net" target="_blank">
    <img src="https://www.univ-reunion.fr/wp-content/uploads/2022/10/Logo_UR.png" width="200" alt="Nuit du Code">
  </a>
</p>
