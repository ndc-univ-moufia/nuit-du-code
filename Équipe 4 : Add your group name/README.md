<p align="center">
  <a href="https://www.univ-reunion.fr" target="_blank">
    <img src="https://www.univ-reunion.fr/wp-content/uploads/2022/10/Logo_UR.png" width="400" alt="Université de la réunion">
  </a>
</p>

## Équipe 4

Bienvenue dans le dossier de l'Équipe 4. Si votre équipe n'a pas encore de nom, veuillez en choisir un et le renseigner
dans le nom de votre dossier.

## Installation de Pyxel

Pour installer Pyxel, utilisez la commande pip suivante :

```bash
pip install pyxel
```

Ou pip3 si vous utilisez Python 3.

````bash
pip3 install pyxel
````

## Documentation

Vous devrez créer une documentation pour votre projet. Cette documentation peut être au format PDF ou au format Markdown
en utilisant ce fichier README.

## Présentation des fichiers

Vous pouvez utiliser ce fichier README pour présenter les différents fichiers de votre projet, y compris votre
documentation si elle est au format PDF. C'est un bon moyen de donner un aperçu de la structure de votre projet et des
fichiers importants.

## Utilisation de Git avec GitLab

Si vous rencontrez des problèmes avec l'utilisation de Git ou GitLab, n'hésitez pas à demander de l'aide. Il est
préférable de demander de l'aide plutôt que de risquer de détruire le travail des autres par erreur.

## Règles

- Pas de copier-coller. Le plagiat n'est pas autorisé.
- Vous ne devez modifier que votre propre dossier.
- Et surtout, amusez-vous !