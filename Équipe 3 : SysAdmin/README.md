# Nuit du Code - 29 mai 2024

<p align="center">
  <a href="https://www.univ-reunion.fr" target="_blank">
    <img src="https://www.univ-reunion.fr/wp-content/uploads/2022/10/Logo_UR.png" width="400" alt="Université de la Réunion">
  </a>
</p>

---

Bienvenue sur notre projet ! Nous sommes l'équipe **SysAdmin** ! Oui oui comme
le métier, connus sous les noms de **Oxi (Jian)** et **Kazurix (Franck)**, nous sommes
tous deux des passionnés d'informatique, nous sommes amenés à administrer,
maintenir des serveurs informatiques. D'où ce choix de nom pour notre équipe.<br>
Nous espérons sincèrement que notre production sera à la hauteur de vos attentes !

## Choix du thème
Nous avons choisi le thème n°1 : **JO-métrique**.<br>
Ce choix s'explique de par le fait que l'utilisation de fichier `.pyxres` n'est
pas autorisé, nous aimons prendre des risques, et c'est un challenge que nous
avons pris.

## Notre jeu : Le 100 mètres aux Jeux olympiques
Notre jeu est le suivant : **Le 100 mètres à la course**.<br>
Le but est d'arriver en premier sur la ligne d'arrivée modélisé par une ligne
à la fin.<br>
Vous pouvez jouer seul ou à deux avec un ami !<br>
Ce jeu, digne des vraies épreuves des Jeux olympiques exigera toute votre
précision.

## Commandes et Interfaces
Pour arriver premier, vous devez utiliser les commandes suivantes :<br>
● Pour commencer le jeu, vous devez appuyer sur la touche `ESPACE`.<br>
● Les commandes pour le joueur 1 sont : `A` et `Z`.<br>
<u>Mais pourquoi deux touches ?</u><br>
**Deux touches par joueurs**, vous devez cliquer aussi vite que possible,
pour des questions d'ergonomie, nous avons opté d'utiliser deux touches
afin de rendre votre course plus agréable.<br>
● Les commandes pour le joueur 2 sont : `L` et `M`.<br>
<p align="center">
    <img src="https://ndc.jian.ovh/images/Main_Screen.png" width="512" alt="Logo SysAdmin">
    <img src="https://ndc.jian.ovh/images/Game_Screen.png" width="512" alt="Logo SysAdmin">
    <img src="https://ndc.jian.ovh/images/win.png" width="512" alt="Logo SysAdmin">
</p>
## Notre ressenti
Pour ma part (Oxi, Jian) :<br>
C'est la première fois que je participe à cet événement, et je ne suis pas déçu.
C'est pour moi une occasion de mettre à profit mes connaissances en Python !

Pour ma part (Kazurix, Franck) :<br>
C'est une expérience enrichissante, j'ai pu apprendre de nouvelles choses et m'amuser surtout

## Un petit bonus...
Pyxel est une librairie Python formidable ! Elle permet notamment plusieurs
types d'exportation d'un jeu. Vous pouvez compiler votre jeu. Mais aussi,
il existe une option permettant d'exporter un programme en `.html`.<br>
En bonus, nous avons décidé d'héberger la page de notre jeu Pyxel à l'adresse
suivante : <br>
[https://ndc.jian.ovh](https://ndc.jian.ovh)<br>
<iframe src="https://ndc.jian.ovh" style="width:128px; height:128px;"></iframe>


---

<p align="center">
  <a href="https://www.nuitducode.net" target="_blank">
    <img src="https://ndc.jian.ovh/images/logo.png" width="808" alt="Logo SysAdmin">
  </a>
</p>
