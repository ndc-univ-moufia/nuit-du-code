import random

import pyxel


class Chrono:
    def __init__(self):
        self.current_time = None
        self.start_time = pyxel.frame_count
        self.is_running = True

    def update(self):
        if self.is_running:
            self.current_time = (pyxel.frame_count - self.start_time) / 30

    def stop(self):
        self.is_running = False


class Player1:
    def __init__(self):
        self.x = 6
        self.y = 39.5
        self.radius = 5
        self.color = 6
        self.cam_x = 90
        self.cam_y = 14
        self.cam_radius = 2
        self.cam_col = 6
        self.chrono = Chrono()

    def update(self):
        self.chrono.update()
        if pyxel.btnp(pyxel.KEY_A) or pyxel.btnp(pyxel.KEY_Z):
            self.x += 1
            self.cam_x += 0.296875 * 1
        if self.x >= 125:
            self.chrono.stop()

    def draw(self):
        pyxel.circ(self.x, self.y, self.radius, self.color)
        pyxel.circ(self.cam_x, self.cam_y, self.cam_radius, self.cam_col)
        if self.x >= 125:
            pyxel.text(4, 39.5, f"Temps Final: {self.chrono.current_time:.2f}s", 8)


class Player2:
    def __init__(self):
        self.x = 6
        self.y = 64.5
        self.radius = 5
        self.color = 3
        self.cam_x = 90 - 2.5
        self.cam_y = 14 + 3.5
        self.cam_radius = 2
        self.cam_col = 3
        self.chrono = Chrono()

    def update(self):
        self.chrono.update()
        if pyxel.btnp(pyxel.KEY_L) or pyxel.btnp(pyxel.KEY_M):
            self.x += 1
            self.cam_x += 0.31640625 * 1
        if self.x >= 125:
            self.chrono.stop()

    def draw(self):
        pyxel.circ(self.x, self.y, self.radius, self.color)
        pyxel.circ(self.cam_x, self.cam_y, self.cam_radius, self.cam_col)
        if self.x >= 125:
            pyxel.text(4, 64.5, f"Temps Final: {self.chrono.current_time:.2f}s", 8)


class Runner1:
    def __init__(self):
        self.x = 1
        self.y = 89.5
        self.radius = 5
        self.color = 1
        self.speed = random.uniform(0.5, 3)
        self.cam_x = 90 - (2.5 * 2)
        self.cam_y = (3.5 * 2) + 14
        self.cam_radius = 2
        self.cam_col = 1
        self.chrono = Chrono()

    def update(self):
        self.chrono.update()
        self.speed = random.uniform(0.1, 0.5)
        self.x += self.speed
        self.cam_x += 0.3359375 * self.speed
        self.chrono.update()
        if self.x >= 125:
            self.chrono.stop()

    def draw(self):
        pyxel.circ(self.x, self.y, self.radius, self.color)
        pyxel.circ(self.cam_x, self.cam_y, self.cam_radius, self.cam_col)
        if self.x >= 125:
            pyxel.text(4, 89.5, f"Temps Final: {self.chrono.current_time:.2f}s", 8)


class Runner2:
    def __init__(self):
        self.x = 1
        self.y = ((29 * 3) + 18) + 9.5
        self.radius = 5
        self.color = 5
        self.speed = random.uniform(0.1, 0.5)
        self.cam_x = 90 - (2.5 * 3)
        self.cam_y = (3.5 * 3) + 14
        self.cam_radius = 2
        self.cam_col = 5
        self.chrono = Chrono()

    def update(self):
        self.chrono.update()
        self.speed = random.uniform(0.1, 0.5)
        self.x += self.speed
        self.cam_x += 0.35546875 * self.speed
        if self.x >= 125:
            self.chrono.stop()

    def draw(self):
        pyxel.circ(self.x, self.y, self.radius, self.color)
        pyxel.circ(self.cam_x, self.cam_y, self.cam_radius, self.cam_col)
        if self.x >= 125:
            pyxel.text(4, ((29 * 3) + 18) + 9.5, f"Temps Final: {self.chrono.current_time:.2f}s", 8)



class App:
    def __init__(self):
        pyxel.init(128, 128, title="NDC 2024")
        self.player1 = Player1()
        self.player2 = Player2()
        self.runner1 = Runner1()
        self.runner2 = Runner2()
        self.game_state = "waiting"
        self.winner = None
        pyxel.run(self.update, self.draw)

    def update(self):
        if self.game_state == "waiting":
            if pyxel.btnp(pyxel.KEY_SPACE):
                self.game_state = "playing"
        elif self.game_state == "playing":
            self.player1.update()
            self.player2.update()
            self.runner1.update()
            self.runner2.update()
            self.check_winner()

    def draw(self):
        if self.game_state == "waiting":
            pyxel.cls(7)  # Draw a white screen
            pyxel.text(30, 60, "Appuyer sur espace", 0)
        elif self.game_state == "playing":
            pyxel.cls(4)
            pyxel.rect(0, 0, 128, 28, 13)
            pyxel.rect(80, 0, 48, 28, 12)
            pyxel.tri(90, 14, 80, 28, 128, 28, 4)
            pyxel.rect(90, 14, 300, 110, 4)
            # Ligne d'arrivée
            pyxel.line(125, 28, 125, 128, 11)
            # bande blanche piste de course
            pyxel.line(0, 27, 128, 27, 7)
            pyxel.line(0, 52, 128, 52, 7)
            pyxel.line(0, 77, 128, 77, 7)
            pyxel.line(0, 102, 128, 102, 7)
            pyxel.line(0, 127, 128, 127, 7)

            # Personnage 1 : France
            pyxel.rect(10, 33, 10, 15, 1)
            pyxel.rect(20, 33, 10, 15, 7)
            pyxel.rect(30, 33, 10, 15, 14)

            # Personnage 2 : Royaume-Uni
            pyxel.rect(10, 58, 30, 5, 14)
            pyxel.rect(10, 63, 30, 5, 7)
            pyxel.rect(10, 68, 30, 5, 1)

            # Personnage 3 (bot) : Italie
            pyxel.rect(10, 83, 10, 15, 11)
            pyxel.rect(20, 83, 10, 15, 7)
            pyxel.rect(30, 83, 10, 15, 14)

            # Personnage 4 (bot) : Allemagne
            pyxel.rect(10, 108, 30, 5, 0)
            pyxel.rect(10, 113, 30, 5, 14)
            pyxel.rect(10, 118, 30, 5, 10)
            # Jo
            pyxel.circb(20, 9, 7, 10)
            pyxel.circb(37, 9, 7, 10)
            pyxel.circb(54, 9, 7, 10)
            pyxel.circb(28, 17, 7, 10)
            pyxel.circb(45, 17, 7, 10)

            self.player1.draw()
            self.player2.draw()
            self.runner1.draw()
            self.runner2.draw()
            if self.winner is not None:
                pyxel.text(5, 5, f"Gagnant: {self.winner}", 0)

    def check_winner(self):
        if self.player1.x >= 125 and self.winner is None:
            self.winner = "France"
        elif self.player2.x >= 125 and self.winner is None:
            self.winner = "Pays-Bas"
        elif self.runner1.x >= 125 and self.winner is None:
            self.winner = "Italie"
        elif self.runner2.x >= 125 and self.winner is None:
            self.winner = "Allemagne"
App()
